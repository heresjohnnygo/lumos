<?php

namespace App\Http\Controllers;

use App\JiraBug;
use App\JiraEpic;
use App\JiraTask;
use App\Services\Jira;
use App\User;
use App\UsersCompetitionResults;

class JiraController extends Controller
{
    public function getData()
    {
        $jira = new Jira();

        $epicsData = $jira->getEpics();
        foreach ($epicsData as $epicData) {
            $epic = JiraEpic::where('jira_id', $epicData->key)->first();
            if (is_null($epic)) {
                $epic = new JiraEpic();
                $epic->jira_id = $epicData->key;
            }

            $epic->name = $epicData->fields->summary;
            $epic->business_value = 100;
            $epic->save();

            $tasksData = $jira->getTasks($epicData->key);
            foreach ($tasksData as $taskData) {
                $task = JiraTask::where('jira_id', $taskData->key)->first();
                if (is_null($task)) {
                    $task = new JiraTask();
                    $task->jira_id = $taskData->key;
                }

                $task->epic_id = $epic->id;
                $task->user = $this->getAssigneeUser($taskData);
                $task->name = $taskData->fields->summary;
                $task->status = $this->getResolution($taskData);
                $task->resolution_date = $this->getResolutionDate($taskData);
                $task->story_points = (int) $taskData->fields->customfield_10016;
                $task->save();
            }
        }

        $bugsData = $jira->getBugs();
        foreach ($bugsData as $bugData) {
            $bug = JiraBug::where('jira_id', $bugData->key)->first();
            if (is_null($bug)) {
                $bug = new JiraBug();
                $bug->jira_id = $bugData->key;
            }

            $bug->user = $this->getAssigneeUser($bugData);
            $bug->name = $bugData->fields->summary;
            $bug->business_value = 0;
            $bug->save();
        }
    }

    public function parseData()
    {
        UsersCompetitionResults::query()->truncate();

        $users = User::all();
        /** @var User $user */
        foreach ($users as $user) {
            $score = 0;
            if ($user->jira_user) {
                $tasksCount = JiraTask::where('user', $user->jira_user)->where('status', 'Done')->count();
                $bugsCount = JiraBug::where('user', $user->jira_user)->count();
                $score = $tasksCount * 10 - $bugsCount;
            }

            $userCompetition = new UsersCompetitionResults();
            $userCompetition->competition_id = 2;
            $userCompetition->user_id = $user->id;
            $userCompetition->score = $score;
            $userCompetition->save();
        }
    }

    private function getResolution(\stdClass $object): string
    {
        $assignee = 'Not Done';
        $assigneeObj = $object->fields->resolution;
        if ($assigneeObj && property_exists($assigneeObj, 'name')) {
            $assignee = $assigneeObj->name;
        }

        return $assignee;
    }

    private function getResolutionDate(\stdClass $object): ?string
    {
        $assignee = null;
        $assigneeObj = $object->fields;
        if ($assigneeObj && property_exists($assigneeObj, 'resolutiondate') && $assigneeObj->resolutiondate) {
            $assignee = date('Y-m-d H:i:s', strtotime($assigneeObj->resolutiondate));
        }

        return $assignee;
    }

    private function getAssigneeUser(\stdClass $object): string
    {
        $assignee = '';
        $assigneeObj = $object->fields->assignee;
        if ($assigneeObj && property_exists($assigneeObj, 'name')) {
            $assignee = $assigneeObj->name;
        }

        return $assignee;
    }
}
