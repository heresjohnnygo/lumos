<?php

namespace App\Http\Controllers;

use App\Team;
use App\User;

class TeamController extends Controller
{
    public function show($id)
    {
        return view('team.profile', ['team' => Team::find($id)]);
    }
}
