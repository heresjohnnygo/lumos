<?php

namespace App\Http\Controllers;

use App\Badge;
use Illuminate\Http\Request;

class BadgeController extends Controller
{

    public function create()
    {
        return \View::make('badge.create');
    }

    public function save(Request $request)
    {
        $badge = new Badge();
        $badge->name = $request->get('name');
        $badge->description = $request->description;
        $badge->img_src = $request->img_src;

        $badge->save();

        return redirect()->route('badge');
    }

    public function index()
    {
        return \View::make('badge.index')->with('badges', Badge::all());
    }
}
