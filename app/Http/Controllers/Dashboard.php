<?php

namespace App\Http\Controllers;

use App\Feeds;
use Illuminate\Http\Request;

class Dashboard extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        $data = [];
        $data['feeds'] = Feeds::where('user_id', $user->id)->get();

        return view('dashboard.index', $data);
    }
}
