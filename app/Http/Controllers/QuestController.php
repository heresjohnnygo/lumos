<?php

namespace App\Http\Controllers;

use App\Badge;
use App\Feeds;
use App\Quest;
use App\QuestBadges;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [];
        $data['quests'] = Quest::all();
        return view('quests.index', $data);
    }

    /**
     * Add Competition.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add()
    {
        $data = [];
        $data['users'] = User::all();
        $data['badges'] = Badge::all();

        return view('quests.add', $data);
    }

    public function save(Request $request)
    {
        $quest = new Quest();
        $quest->name = $request->get('name');
        $quest->start_date = date('Y-m-d', strtotime($request->get('start_date')));
        $quest->end_date = date('Y-m-d', strtotime($request->get('end_date')));
        $quest->user_id = $request->get('user');
        $quest->save();

        foreach($request->get('badges') as $badge) {
            $questBadges = new QuestBadges();
            $questBadges->badge_id = $badge;
            $questBadges->quest_id = $quest->id;
            $questBadges->achieved = 0;
            $questBadges->save();
        }

        return redirect()->route('quest');
    }

    public function details(Request $request, $id)
    {
        $data = [];
        $data['quest'] = Quest::find($id);
        $badges = DB::table('quest_badges')
            ->join('badges', 'quest_badges.badge_id', '=', 'badges.id')
            ->select(['badges.*', 'quest_badges.achieved', 'quest_badges.id as questBadgeId'])
            ->where('quest_id', '=' , $id)
            ->get();
        $data['badges'] = $badges;

        return view('quests.details', $data);
    }

    public function giveaway(Request $request, $id)
    {
       $questBadges = QuestBadges::find($id);
       $questBadges->achieved = 1;
       $questBadges->save();

       $badge = Badge::find($questBadges->badge_id);
       $quest = Quest::find($questBadges->quest_id);

       $feed = new Feeds();
       $feed->date = date('Y-m-d H:i:s');
       $feed->title = "You have received a " . $badge->name . "!";
       $feed->badge = $badge->img_src;
       $feed->user_id = $quest->user_id;
       $feed->save();

       return redirect()->route('questDetails', ['id' => $quest->id]);
    }


}
