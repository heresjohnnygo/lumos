<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    public function show($id)
    {
        return view('user.profile', ['user' => User::find($id)]);
    }

    public function showAll()
    {
        return view('user.users', ['users' => User::all()]);
    }

    public function edit(User $user)
    {
        return view('user.edit', compact('user'));
    }

    public function update(User $user)
    {
//        $this->validate(request(), [
//            'name' => 'required',
//            'email' => 'required|email|unique:users',
//            'password' => 'required|min:6|confirmed'
//        ]);

        $user->name = request('name');
        $user->email = request('email');
        //$user->password = bcrypt(request('password'));

        $user->save();

        return redirect('users')->with('success','User was updated !');
    }
}
