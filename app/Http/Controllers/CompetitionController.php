<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Team;
use App\TeamCompetitionResults;
use App\UserCompetitionResults;
use Illuminate\Http\Request;

class CompetitionController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [];
        $data['competitions'] = Competition::all();
        return view('competition.index', $data);
    }

    /**
     * Add Competition.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add()
    {
        $data = [];
        $data['teams'] = Team::all();

        return view('competition.add', $data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function details(Request $request, $id)
    {
        $data = [];
        $data['competition'] = Competition::find($id);
        $data['winner'] = [];
        $data['competition_results'] = [];
        if($data['competition']['type'] == 0) {
            $competitionResults = TeamCompetitionResults::with('team')->where('competition_id', $id)->get()->sortByDesc('score');

            foreach($competitionResults as $result) {
                if(empty($data['winner']) && $result->score>=$data['competition']['target_value']){
                    $data['winner'] = ['name' => $result->team->name];
                }

                $data['competition_results'][] = ['name' => $result->team->name, 'score' => $result->score];
            }
        } else {

            $competitionResults = UserCompetitionResults::with('user')->where('competition_id', $id)->get()->sortByDesc('score');
            foreach($competitionResults as $result) {
                if(empty($data['winner']) && $result->score>=$data['competition']['target_value']){
                    $data['winner'] = ['name' => $result->user->name];
                }

                $data['competition_results'][] = ['name' => $result->user->name, 'score' => $result->score];
            }
        }

        return view('competition.details', $data);
    }

    public function save(Request $request)
    {
        $competition = new Competition();
        $competition->name = $request->get('name');
        $competition->description = $request->get('description');
        $competition->start_date = date('Y-m-d', strtotime($request->get('start_date')));
        $competition->end_date = date('Y-m-d', strtotime($request->get('end_date')));
        $competition->type = $request->get('type');
        $competition->target_value = $request->get('target_value');
        $competition->team = $request->get('teams');
        $competition->prize = $request->get('prize');

        $competition->save();

        return redirect()->route('competition');
    }
}
