<?php


namespace App\Services;


use GuzzleHttp\Client;

class GetStackOverflowScore
{

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function get(string $userId): float
    {
        $response = $this->client->get("https://api.stackexchange.com/2.2/users/$userId?site=stackoverflow");
        $responseAsObj = json_decode($response->getBody());

        if (!empty($responseAsObj->items)) {
            return $responseAsObj->items[0]->reputation;
        }

        return 0;

    }
}
