<?php

namespace App\Services;

use GuzzleHttp\Client;

class Jira
{
    private $user = 'alexandru.petcu@outlook.com';
    private $password = '196P6wS6vbsOgsMjcCm9A336';
    private $client;
    private $url = 'https://lumoshackathon.atlassian.net//rest/api/2/search';

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getEpics(): array
    {
        return $this->getData('issuetype="Epic"')->issues;
    }

    public function getTasks(string $key): array
    {
        return $this->getData('parent[key]='.$key)->issues;
    }

    public function getBugs(): array
    {
        return $this->getData('issuetype="Bug"')->issues;
    }

    private function getData(string $query): \stdClass
    {
        $response = $this->client->get($this->url.'?jql='.$query, [
            'auth' => [$this->user, $this->password],
        ]);

        return json_decode($response->getBody());
    }
}
