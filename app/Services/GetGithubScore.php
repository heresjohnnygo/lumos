<?php


namespace App\Services;


use GuzzleHttp\Client;

class GetGithubScore
{

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function get(string $user): float
    {
        $response = $this->client->get("http://api.github.com/search/users?q=$user");
        $responseAsObj = json_decode($response->getBody());


        if ($responseAsObj->total_count >= 1) {
            return $responseAsObj->items[0]->score;
        }

        return 0;

    }
}
