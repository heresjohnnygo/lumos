<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function manager()
    {
        return $this->belongsTo(User::class);
    }

    public function members()
    {
        return $this->belongsToMany(User::class, 'team_user')->withTimestamps();
    }

    public function sprints()
    {
        $this->belongsTo(Sprint::class);
    }
}
