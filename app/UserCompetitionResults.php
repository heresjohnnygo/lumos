<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCompetitionResults extends Model
{
    protected $table = 'users_competition_results';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
