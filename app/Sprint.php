<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sprint extends Model
{
    public function team()
    {
        return $this->hasOne(Team::class);
    }

    //todo add relation with jira epic
}
