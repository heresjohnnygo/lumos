<?php

namespace App\Console\Commands;

use App\Services\GetGithubScore;
use App\Services\GetStackOverflowScore;
use App\User;
use Illuminate\Console\Command;

class GetStackOverflowScores extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lumos:get-stackoverflow-scores';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $getStackOverflowScore;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GetStackOverflowScore $getStackOverflowScore)
    {
        parent::__construct();

        $this->getStackOverflowScore = $getStackOverflowScore;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::all()->each(function (User $user) {
            if ($user->hasStackOverflow()) {
                $user->stack_overflow_score = $this->getStackOverflowScore->get($user->stack_overflow_userid);
                $user->save();
            }
        });
    }
}
