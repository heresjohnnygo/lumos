<?php

namespace App\Console\Commands;

use App\Services\GetGithubScore;
use App\User;
use Illuminate\Console\Command;

class GetGithubScores extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lumos:get-github-scores';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $getGithubScore;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GetGithubScore $getStackOverflowScore)
    {
        parent::__construct();

        $this->getGithubScore = $getStackOverflowScore;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::all()->each(function (User $user) {
            if ($user->hasGithub()) {
                $user->github_score = $this->getGithubScore->get($user->github_user);
                $user->save();
            }
        });
    }
}
