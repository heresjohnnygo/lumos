<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    public function team()
    {
        return $this->hasOne(Team::class);
    }

    public function teamSBusinessValue()
    {
        return $this->sprints()->sum(function (Sprint $sprint) {
            //todo iterate over the epics that are in the sprints and sum the epics business values
            //smth like
            return $sprint->epics->sum(function (JiraEpic $jiraEpic) {
                return $jiraEpic->business_value;
            });
        });
    }

    public function sprints()
    {
        //todo need to be tested
        return Sprint::all()->where('started_at', '>=', $this->start_date)
            ->where('ended_at', '<=', $this->end_date)->get();
    }

    public function nSprintsInARow()
    {
        //todo need to be tested
        $i = 0;
        foreach ($this->sprints() as $sprint) {
            if ($sprint->status == 0) {
                return;
            }
            $i++;
        }

        return $i;
    }


}
