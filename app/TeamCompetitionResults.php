<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamCompetitionResults extends Model
{
    protected $table = 'team_competition_results';

    public function team()
    {
        return $this->belongsTo('App\Team');
    }
}
