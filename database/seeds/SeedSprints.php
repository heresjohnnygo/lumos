<?php

use App\Sprint;
use App\Team;
use Illuminate\Database\Seeder;

class SeedSprints extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sprint = new Sprint();
        $sprint->name = 'S1';
        $sprint->startedAt = Carbon\Carbon::now()->subWeek(2);
        $sprint->endedAt = Carbon\Carbon::now();
        $sprint->status = true;
        $sprint->team_id = (Team::where('name', 'Targaryen')->get()->first())->id;
        $sprint->save();


        $sprint = new Sprint();
        $sprint->name = 'S2';
        $sprint->startedAt = Carbon\Carbon::now()->subWeek(4);
        $sprint->endedAt = Carbon\Carbon::now(2);
        $sprint->status = false;
        $sprint->team_id = (Team::where('name', 'Targaryen')->get()->first())->id;
        $sprint->save();

        $sprint = new Sprint();
        $sprint->name = 'S3';
        $sprint->startedAt = Carbon\Carbon::now()->subWeek(6);
        $sprint->endedAt = Carbon\Carbon::now(4);
        $sprint->team_id = (Team::where('name', 'Stark')->get()->first())->id;
        $sprint->status = true;
        $sprint->save();
    }
}
