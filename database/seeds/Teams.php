<?php

use App\Team;
use App\User;
use Illuminate\Database\Seeder;

class Teams extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $team=new Team();
        $team->name='Targaryen';
        $team->manager_id=(User::where('email','mihai.leu@emag.ro')->first())->id;

        $team->save();

        $team->members()->save(User::where('email','radu.tiganuc@emag.ro')->first());
    }
}
