<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBugs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jira_bugs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jira_id');
            $table->string('name');
            $table->integer('business_value');
            $table->timestamps();

            $table->unique('jira_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jira_bugs');
    }
}
