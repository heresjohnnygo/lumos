<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jira_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jira_id');
            $table->unsignedBigInteger('epic_id');
            $table->string('name');
            $table->string('assignee');
            $table->string('status');
            $table->integer('story_points');
            $table->timestamps();

            $table->foreign('epic_id')
                ->references('id')->on('jira_epics');

            $table->unique('jira_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jira_tasks');
    }
}
