
<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__body kt-portlet__body--fit">

        <!--begin::Widget -->
        <div class="kt-widget kt-widget--project-1">
            <div class="kt-widget__head">
                <div class="kt-widget__label">
                    <div class="kt-widget__media">
                            <span class="kt-media kt-media--lg kt-media--circle">

                            </span>
                    </div>
                    <div class="kt-widget__info kt-margin-t-5">
                        <a href="#" class="kt-widget__title">
                            {{$name}}
                        </a>
                    </div>
                </div>
            </div>
            <div class="kt-widget__body" style="padding-bottom:0px">
                <div class="kt-widget__stats">
                    <div class="kt-widget__item">
                            <span class="kt-widget__date">
                                Start Date
                            </span>
                        <div class="kt-widget__label">
                            <span class="btn btn-label-brand btn-sm btn-bold btn-upper">{{date('d M, y', strtotime($start_date))}}</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <span class="kt-widget__date">
                            Due Date
                        </span>
                        <div class="kt-widget__label">
                            <span class="btn btn-label-danger btn-sm btn-bold btn-upper">{{date('d M, y', strtotime($end_date))}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-widget__footer">
                <div class="kt-widget__wrapper">
                    <div class="kt-widget__section">

                    </div>
                    <div class="kt-widget__section">
                        <a href="{{url('/quest/details/' . $id)}}"  class="btn btn-brand btn-sm btn-upper btn-bold">details</a>
                    </div>
                </div>
            </div>
        </div>

        <!--end::Widget -->
    </div>
</div>
