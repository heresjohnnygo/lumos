@extends('layouts.app')

@section('content')
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Quest Details </h3>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="#" class="kt-subheader__breadcrumbs-link">
                        Quests </a>

                </div>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Row-->
        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div class="kt-portlet ">
                    <div class="kt-portlet__body">
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top">
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <a href="#" class="kt-widget__title" style="font-size:19px">{{$quest['name']}}</a>

                                    </div>

                                    <div class="kt-widget__info">
                                        <div class="kt-widget__stats d-flex align-items-center flex-fill">
                                            <div class="kt-widget__item">
																<span class="kt-widget__date">
																	Start Date
																</span>
                                                <div class="kt-widget__label">
                                                    <span class="btn btn-label-brand btn-sm btn-bold btn-upper">{{date('d M, y', strtotime($quest['start_date']))}}</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
																<span class="kt-widget__date">
																	Due Date
																</span>
                                                <div class="kt-widget__label">
                                                    <span class="btn btn-label-danger btn-sm btn-bold btn-upper">{{date('d M, y', strtotime($quest['end_date']))}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget__bottom">
                                <div class="kt-widget__item">
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Badges</span>
                                        <span class="kt-widget__text"></span>
                                        <div class="row">
                                        @foreach($badges as $badge)
                                            <div class="col-lg-3">
                                                <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                                                    <div class="kt-portlet__head kt-portlet__head--noborder">
                                                        <div class="kt-portlet__head-label">
                                                            <h3 class="kt-portlet__head-title">
                                                                {{$badge->name}}
                                                            </h3>
                                                        </div>
                                                        <div class="kt-portlet__head-toolbar">
                                                            @if($badge->achieved == 0)<a href="{{ url('/quest/giveaway/' . $badge->questBadgeId) }}" class="btn btn-sm btn-danger" style="color:white">Give Badge</a>@endif
                                                        </div>
                                                    </div>
                                                    <div class="kt-portlet__body" style="align-content: center">
                                                        <img src="{{asset($badge->img_src)}}" style="width:200px;margin-left:auto;margin-right:auto; @if($badge->achieved == 0) filter: grayscale(100%); @endif "/>
                                                    </div>
                                                    <div class="kt-portlet__foot">
                                                        {{$badge->description}}
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection