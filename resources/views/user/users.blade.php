@extends('layouts.app')

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <div class="kt-subheader__breadcrumbs">
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions"
                         data-placement="top">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Row-->
        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            User List
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">


                                    <!--begin::Section-->

                                            <table class="table" style="width:1220px">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Full Name</th>
                                                    <th>Work email</th>
                                                    @foreach ($users as $user)
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th scope="row"></th>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>
                                                        <a onclick="window.location='{{ url("/users/$user->id/edit") }}'"
                                                           class="btn btn-default kt-subheader__btn-options"
                                                           aria-haspopup="true" aria-expanded="false"><i
                                                                class="la la-plus"></i>
                                                            Edit User
                                                        </a>
                                                    </td>
                                                    @endforeach
                                                </tr>
                                                </tbody>
                                            </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
