@extends('layouts.app')

@section('content')
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Badges </h3>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="#" class="kt-subheader__breadcrumbs-link">
                        Badges </a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions"
                         data-placement="top">
                        <a href="{{ url('/badges/add') }}" class="btn btn-danger kt-subheader__btn-options"
                           aria-haspopup="true" aria-expanded="false"><i class="la la-plus"></i>
                            Add Badge
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Row-->
        <div class="row">
            @foreach($badges as $badge)
                <div class="col-lg-4 col-xl-4">
                    @include('badge.badge', $badge)
                </div>
            @endforeach
        </div>
    </div>
@endsection