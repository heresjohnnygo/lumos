<div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
    <div class="kt-portlet__head kt-portlet__head--noborder">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{$name}}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body" style="align-content: center">
        <img src="{{asset($img_src)}}" style="width:200px;margin-left:20%"/>
    </div>
    <div class="kt-portlet__foot">
        {{$description}}
    </div>
</div>