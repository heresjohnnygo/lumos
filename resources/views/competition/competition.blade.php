
    <div class="kt-portlet kt-portlet--height-fluid">
        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin::Widget -->
            <div class="kt-widget kt-widget--project-1">
                <div class="kt-widget__head">
                    <div class="kt-widget__label">
                        <div class="kt-widget__media">
                            <span class="kt-media kt-media--lg kt-media--circle">

                            </span>
                        </div>
                        <div class="kt-widget__info kt-margin-t-5">
                            <a href="#" class="kt-widget__title">
                                {{$name}}
                            </a>
                            <span class="kt-widget__desc">
																	{{$description}}
																</span>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__body" style="padding-bottom:0px">
                    <div class="kt-widget__stats">
                        <div class="kt-widget__item">
																<span class="kt-widget__date">
																	Start Date
																</span>
                            <div class="kt-widget__label">
                                <span class="btn btn-label-brand btn-sm btn-bold btn-upper">{{date('d M, y', strtotime($start_date))}}</span>
                            </div>
                        </div>
                        <div class="kt-widget__item">
																<span class="kt-widget__date">
																	Due Date
																</span>
                            <div class="kt-widget__label">
                                <span class="btn btn-label-danger btn-sm btn-bold btn-upper">{{date('d M, y', strtotime($end_date))}}</span>
                            </div>
                        </div>
                        <div class="kt-widget__item flex-fill">
                            <span class="kt-widget__subtitel">Progress</span>
                            <div class="kt-widget__progress d-flex  align-items-center">
                                <div class="progress" style="height: 5px;width: 100%;">
                                    <div class="progress-bar kt-bg-warning" role="progressbar" style="width: {{$progress}}%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="kt-widget__stat">
																		{{$progress}} %
																	</span>
                            </div>
                        </div>
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__details">
                            <span class="kt-widget__subtitle">Prize</span>
                            <div class="kt-media-group">
                                <span class="kt-widget__text" style="margin-top:0px !important;">{{$prize}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__footer">
                    <div class="kt-widget__wrapper">
                        <div class="kt-widget__section">

                        </div>
                        <div class="kt-widget__section">
                            <a href="{{url('/competition/details/' . $id)}}"  class="btn btn-brand btn-sm btn-upper btn-bold">details</a>
                        </div>
                    </div>
                </div>
            </div>

            <!--end::Widget -->
        </div>
    </div>
