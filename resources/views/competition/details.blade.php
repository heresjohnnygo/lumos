@extends('layouts.app')

@section('content')
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Competition Details </h3>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="#" class="kt-subheader__breadcrumbs-link">
                        Competitions </a>

                </div>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Row-->
        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div class="kt-portlet ">
                    <div class="kt-portlet__body">
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top">
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <a href="#" class="kt-widget__title" style="font-size:19px">{{$competition['name']}}</a>

                                    </div>

                                    <div class="kt-widget__info">
                                        <div class="kt-widget__desc">
                                            {{$competition['description']}}
                                        </div>
                                        <div class="kt-widget__stats d-flex align-items-center flex-fill">
                                            <div class="kt-widget__item">
																<span class="kt-widget__date">
																	Start Date
																</span>
                                                <div class="kt-widget__label">
                                                    <span class="btn btn-label-brand btn-sm btn-bold btn-upper">{{date('d M, y', strtotime($competition['start_date']))}}</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
																<span class="kt-widget__date">
																	Due Date
																</span>
                                                <div class="kt-widget__label">
                                                    <span class="btn btn-label-danger btn-sm btn-bold btn-upper">{{date('d M, y', strtotime($competition['end_date']))}}</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item flex-fill">
                                                <span class="kt-widget__subtitel">Progress</span>
                                                <div class="kt-widget__progress d-flex  align-items-center">
                                                    <div class="progress" style="height: 5px;width: 100%; @if(!empty($winner)) background-color:green; @endif">
                                                        <div class="progress-bar kt-bg-success" role="progressbar" style="width: @if(empty($winner)) {{$competition['progress'] . '%'}} @else 100% @endif;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="kt-widget__stat">
                                                        @if(empty($winner))
																		{{$competition['progress']}}%
                                                            @else
                                                                100%
                                                        @endif
																	</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget__bottom">
                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-confetti"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Winner</span>
                                        <span class="kt-widget__text">@if(!empty($winner)) {{$winner['name']}} @endif</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Rankings
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget2_tab1_content">
                                <div class="kt-widget2">
                                    @foreach($competition_results as $results)
                                    <div class="kt-widget2__item kt-widget2__item--primary">
                                        <div class="kt-widget2__info">
                                            <a href="#" class="kt-widget2__title" style="padding-left:50px">
                                                {{$results['name']}}
                                            </a>
                                        </div>
                                        <div class="kt-widget2__actions">
                                           {{$results['score']}}
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection