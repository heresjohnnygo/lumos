@extends('layouts.app')

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Add New Competition </h3>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Competition </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="#" class="kt-subheader__breadcrumbs-link">
                        Add </a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">

            </div>
        </div>
    </div>

    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Row-->
        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                    <div class="kt-portlet__body">
                        <form class="kt-form" action="{{url('/competition/save')}}" method="POST">
                            @csrf
                            <div class="kt-portlet__body">
                                <div class="kt-section kt-section--first">
                                    <div class="form-group">
                                        <label>Competition Name:</label>
                                        <input type="text" class="form-control" placeholder="Enter full name" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Competition Type:</label>
                                        <select class="form-control" name="type">
                                            <option>Select</option>
                                            <option value="1">N sprint in a row</option>
                                            <option value="2">Number of closed stories</option>
                                            <option value="3">Number of closed bugs </option>
                                            <option value="4">Number of total story points </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Target Value:</label>
                                        <input type="text" class="form-control" placeholder="Target value" name="target_value">
                                    </div>
                                    <div class="form-group">
                                        <label>Start Date:</label>
                                        <input type="date" class="form-control" placeholder="Start date" name="start_date">
                                    </div>
                                    <div class="form-group">
                                        <label>End Date:</label>
                                        <input type="date" class="form-control" placeholder="End date" name="end_date">
                                    </div>

                                    <div class="form-group">
                                        <label>Teams:</label>
                                        <select name='teams' class="form-control" id="teams">
                                            <option value="0">All</option>
                                            @foreach($teams as $item)
                                                <option value="{{$item['id']}}">{{$item['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Prize</label>
                                        <textarea class="form-control" name="prize"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <a href="{{ url('/competition') }}" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function() {
            
        })
    </script>
@endsection;

