<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Badge;
use App\Http\Controllers\BadgeController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/jira/get-data', 'JiraController@getData')->name('jira_get_data');
Route::get('/jira/parse-data', 'JiraController@parseData')->name('jira_parse_data');
Route::get('users/{id}', 'UserController@show');
Route::get('team/{id}', 'TeamController@show');
Route::get('/users', 'UserController@showAll');
Route::get('/dashboard', 'Dashboard@index')->name('dashboard');
Route::get('/users/{user}/edit',  ['as' => 'users.edit', 'uses' => 'UserController@edit']);
Route::patch('/users/{user}',  ['as' => 'users.update', 'uses' => 'UserController@update']);
Route::get('/competition', 'CompetitionController@index')->name('competition');

Route::get('/competition/add', 'CompetitionController@add')->name('addCompetition');
Route::post('/competition/save', 'CompetitionController@save')->name('saveCompetition');
Route::get('/competition/details/{id}', 'CompetitionController@details')->name('addCompetition');
Route::get('/badges/add', 'BadgeController@create');
Route::get('/badge', 'BadgeController@index')->name('badge');
Route::post('/badges/save', 'BadgeController@save')->name('badges.save');
Route::get('/quest', 'QuestController@index')->name('quest');
Route::get('/quest/add', 'QuestController@add')->name('addQuest');
Route::post('/quest/save', 'QuestController@save')->name('saveQuest');
Route::get('/quest/details/{id}', 'QuestController@details')->name('questDetails');
Route::get('/quest/giveaway/{id}', 'QuestController@giveaway')->name('giveaway');
